package org.mozilla.materialfennec;

import android.support.test.espresso.web.webdriver.Locator;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.test.suitebuilder.annotation.LargeTest;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ListView;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.annotation.Nullable;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.pressKey;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static android.support.test.espresso.web.assertion.WebViewAssertions.webMatches;
import static android.support.test.espresso.web.sugar.Web.onWebView;
import static android.support.test.espresso.web.webdriver.DriverAtoms.findElement;
import static android.support.test.espresso.web.webdriver.DriverAtoms.getText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class SuggestionTest {

    private final static String SEARCH_FIREFOX = "Firefox";
    private final static String MOCK_FILE_URI = "file:///android_asset/firefox.html";

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void display_Switch_Menu() {
        onView(withId(R.id.switcher)).check(matches(isDisplayed()));
        onView(withId(R.id.menu)).check(matches(isDisplayed()));
    }

    @Test
    public void display_clear() {
        onView(withId(R.id.switcher)).check(matches(isDisplayed()));
        onView(withId(R.id.menu)).check(matches(isDisplayed()));

        onView(withId(R.id.url)).perform(click());

        onView(withId(R.id.switcher)).check(matches(not(isDisplayed())));
        onView(withId(R.id.menu)).check(matches(not(isDisplayed())));
    }

    @Test
    public void display_text_and_clear() {
        onView(withId(R.id.url)).perform(click());

        onView(withId(R.id.url))
                .perform(replaceText(SEARCH_FIREFOX), closeSoftKeyboard())
                .check(matches(withText(SEARCH_FIREFOX)));

        onView(withId(R.id.clear)).perform(click());

        onView(withId(R.id.url)).check(matches(not(withText((String) null))));

        onView(withId(R.id.clear)).perform(click());

        onView(withId(R.id.switcher)).check(matches(isDisplayed()));
        onView(withId(R.id.menu)).check(matches(isDisplayed()));
    }

    @Test
    public void suggest_firefox() {
        onView(withId(R.id.url)).perform((click()));

        onView(withId(R.id.url))
                .perform(replaceText(SEARCH_FIREFOX), closeSoftKeyboard())
                .check(matches(withText(SEARCH_FIREFOX)));

        onView(withItemText(SEARCH_FIREFOX.toLowerCase())).perform(click());

        onView(withId(R.id.recyclerView)).check(matches(not(isDisplayed())));

        onWebView(withId(R.id.webview))
                .withElement(findElement(Locator.TAG_NAME, "b"))
                .check(webMatches(getText(), containsString(SEARCH_FIREFOX)));
    }

    @Test
    public void suggest_mock_page() {
        onView(withId(R.id.url))
                .perform(typeText(MOCK_FILE_URI), pressKey(KeyEvent.KEYCODE_ENTER));
        onWebView(withId(R.id.webview))
                .withElement(findElement(Locator.TAG_NAME, "b"))
                .check(webMatches(getText(), containsString(SEARCH_FIREFOX)));
    }

    /**
     * Ensures the truth of an expression involving one or more parameters to the calling method.
     *
     * @param expression   a boolean expression
     * @param errorMessage the exception message to use if the check fails; will be converted to a
     *                     string using {@link String#valueOf(Object)}
     * @throws IllegalArgumentException if {@code expression} is false
     */
    private static void checkArgument(boolean expression, @Nullable Object errorMessage) {
        if (!expression) {
            throw new IllegalArgumentException(String.valueOf(errorMessage));
        }
    }

    /**
     * A custom {@link Matcher} which matches an item in a {@link ListView} by its text.
     * <p/>
     * View constraints:
     * <ul>
     * <li>View must be a child of a {@link ListView}
     * <ul>
     *
     * @param itemText the text to match
     * @return Matcher that matches text in the given view
     */
    private Matcher<View> withItemText(final String itemText) {
        checkArgument(!TextUtils.isEmpty(itemText), "itemText cannot be null or empty");
        return new TypeSafeMatcher<View>() {
            @Override
            public boolean matchesSafely(View item) {
                return allOf(
                        isDescendantOfA(isAssignableFrom(RecyclerView.class)),
                        withText(itemText)).matches(item);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("is isDescendantOfA LV with text " + itemText);
            }
        };
    }


}
