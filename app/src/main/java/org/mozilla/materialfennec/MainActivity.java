package org.mozilla.materialfennec;

import android.animation.LayoutTransition;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private EditText urlView;
    private ImageView menuView;
    private ImageView switchView;
    private CardView cardView;
    private ViewPager pagerView;
    private LinearLayout containerView;
    private ImageView clearView;
    private TabLayout tabsView;
    private LinearLayout homePanel;
    private WebView webView;
    private RecyclerView recyclerView;

    private SuggestionPresenter suggestionPresenter;

    private int containerPadding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        containerPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics());

        containerView = (LinearLayout) findViewById(R.id.container);
        cardView = (CardView) findViewById(R.id.card);
        urlView = (EditText) findViewById(R.id.url);
        menuView = (ImageView) findViewById(R.id.menu);
        switchView = (ImageView) findViewById(R.id.switcher);
        pagerView = (ViewPager) findViewById(R.id.pager);
        clearView = (ImageView) findViewById(R.id.clear);
        tabsView = (TabLayout) findViewById(R.id.tabs);
        homePanel = (LinearLayout) findViewById(R.id.home_panel);
        webView = (WebView) findViewById(R.id.webview);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        clearView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(urlView.getText())) {
                    urlView.clearFocus();
                } else {
                    urlView.setText(null);
                }
            }
        });

        urlView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                switchView.setVisibility(hasFocus ? View.GONE : View.VISIBLE);
                menuView.setVisibility(hasFocus ? View.GONE : View.VISIBLE);
                recyclerView.setVisibility(hasFocus ? View.VISIBLE : View.GONE);
            }
        });

        LayoutTransition transition = containerView.getLayoutTransition();

        transition.addTransitionListener(new LayoutTransition.TransitionListener() {
            @Override
            public void startTransition(LayoutTransition transition, ViewGroup container, View view, int transitionType) {
                if (!urlView.hasFocus()) {
                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(urlView.getWindowToken(), 0);

                    clearView.setVisibility(View.GONE);

                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) cardView.getLayoutParams();
                    params.setMargins(containerPadding, containerPadding, containerPadding, containerPadding);
                    cardView.setLayoutParams(params);

                    CharSequence url = urlView.getText();
                    if (TextUtils.isEmpty(url)) {
                        webView.setVisibility(View.GONE);
                        homePanel.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void endTransition(LayoutTransition transition, ViewGroup container, View view, int transitionType) {
                if (urlView.hasFocus()) {
                    clearView.setVisibility(View.VISIBLE);

                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) cardView.getLayoutParams();
                    params.setMargins(0, 0, 0, 0);
                    cardView.setLayoutParams(params);
                }
            }
        });

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                urlView.setText(url);
                view.loadUrl(url);
                return true;
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                String url = request.getUrl().toString();
                return shouldOverrideUrlLoading(webView, url);
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        SuggestionContract.OnSuggestionClicked onSuggestionClicked = new SuggestionContract.OnSuggestionClicked() {
            @Override
            public void onClick(CharSequence suggestText, String searchUrl) {
                urlView.setText(suggestText);
                loadUrl(searchUrl);
            }
        };

        SuggestionAdapter suggestionAdapter = new SuggestionAdapter();
        suggestionPresenter = new SuggestionPresenter(suggestionAdapter, new SuggestionDataSource(), onSuggestionClicked);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(suggestionAdapter);

        urlView.setOnEditorActionListener(new OnLoadUrlListener());
        urlView.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                suggestionPresenter.cancelAll();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                suggestionPresenter.querySuggestion(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        pagerView.setAdapter(new HomeAdapter(getSupportFragmentManager()));

        tabsView.setupWithViewPager(pagerView);

        transition.setDuration(100);

        handleIntent(getIntent());
    }

    private void loadUrl(String url) {
        urlView.clearFocus();
        webView.loadUrl(url);
        webView.setVisibility(View.VISIBLE);
        homePanel.setVisibility(View.GONE);
    }

    private void handleIntent(Intent intent) {
        if (intent == null) {
            return;
        }
        if (!Intent.ACTION_VIEW.equals(intent.getAction())) {
            return;
        }
        Uri uri = intent.getData();
        if (uri == null) {
            return;
        }

        loadUrl(uri.toString());
    }

    @Override
    protected void onStop() {
        super.onStop();
        suggestionPresenter.cancelAll();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        suggestionPresenter.stop();
    }

    @Override
    public void onBackPressed() {
        if (urlView.hasFocus()) {
            urlView.clearFocus();
        } else {
            super.onBackPressed();
        }
    }

    public static class HomeAdapter extends FragmentPagerAdapter {
        private static HomePanel[] panels = new HomePanel[]{
                new TopSitesFragment(),
                DummyFragment.create("Bookmarks"),
                DummyFragment.create("History"),
                DummyFragment.create("Reading List"),
                DummyFragment.create("Recent Tabs"),
        };

        public HomeAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return (Fragment) panels[position];
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return panels[position].getTitle();
        }

        @Override
        public int getCount() {
            return panels.length;
        }
    }

    private class OnLoadUrlListener implements TextView.OnEditorActionListener {
        @Override
        public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            CharSequence url = textView.getText();
            if (TextUtils.isEmpty(url)) {
                webView.setVisibility(View.GONE);
                homePanel.setVisibility(View.VISIBLE);
                return false;
            }

            suggestionPresenter.search(url);
            return false;
        }
    }
}
