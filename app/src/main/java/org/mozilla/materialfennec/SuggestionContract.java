package org.mozilla.materialfennec;

interface SuggestionContract {

    interface View {
        void setPresenter(Presenter presenter);

        void updateSuggestion(String[] suggestions);
    }

    interface Presenter {
        void querySuggestion(CharSequence queryText);

        void search(CharSequence searchString);

        void cancelAll();

        void stop();
    }

    interface Model {
        void query(CharSequence query, QueryReponseListener listener);

        String searchUrl(CharSequence searchUrl);
    }

    interface OnSuggestionClicked {
        void onClick(CharSequence suggestText, String searchUrl);
    }

    interface QueryReponseListener {
        void onQuerySuccess(String[] suggestions);
    }
}
