package org.mozilla.materialfennec;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

class SuggestionAdapter extends RecyclerView.Adapter<SuggestionAdapter.ViewHolder> implements SuggestionContract.View {

    private SuggestionContract.Presenter mPresenter;

    public SuggestionAdapter() {
    }

    String[] suggest = new String[]{};

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.suggestion, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return suggest.length;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String text = suggest[position];
        holder.bind(text);
    }

    @Override
    public void setPresenter(SuggestionContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void updateSuggestion(String[] suggestions) {
        suggest = suggestions;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView mTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            mTextView = (TextView) itemView.findViewById(R.id.suggest_title);
            itemView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPresenter.search(mTextView.getText());
                }
            });
        }

        void bind(String text) {
            mTextView.setText(text);
        }
    }

}
