package org.mozilla.materialfennec;

import android.text.TextUtils;

class SuggestionPresenter implements SuggestionContract.Presenter {

    private final SuggestionContract.View mSuggestionView;
    private final SuggestionDataSource mSuggestionDataSource;
    private final SuggestionContract.OnSuggestionClicked mOnSuggestionClicked;

    SuggestionPresenter(SuggestionContract.View suggestionView, SuggestionDataSource suggestionDataSource, SuggestionContract.OnSuggestionClicked onSuggestionClicked) {
        mSuggestionView = suggestionView;
        mSuggestionDataSource = suggestionDataSource;
        mOnSuggestionClicked = onSuggestionClicked;

        mSuggestionView.setPresenter(this);
    }

    @Override
    public void querySuggestion(final CharSequence queryText) {
        if (TextUtils.isEmpty(queryText)) {
            mSuggestionView.updateSuggestion(new String[0]);
            return;
        }

        mSuggestionDataSource.query(queryText, new SuggestionContract.QueryReponseListener() {
            @Override
            public void onQuerySuccess(String[] suggestions) {
                mSuggestionView.updateSuggestion(suggestions);
            }
        });
    }

    @Override
    public void search(CharSequence searchString) {
        String url = searchString.toString();

        if (!url.startsWith("http://") && !url.startsWith("https://") && !url.startsWith("file:///")) {
            mOnSuggestionClicked.onClick(searchString, mSuggestionDataSource.searchUrl(url));
        } else {
            mOnSuggestionClicked.onClick(searchString, url);
        }

    }

    @Override
    public void cancelAll() {
        mSuggestionDataSource.cancelAll();
    }

    @Override
    public void stop() {
        mSuggestionDataSource.stop();
    }


}
