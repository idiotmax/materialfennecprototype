package org.mozilla.materialfennec;

import android.os.AsyncTask;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

class SuggestionDataSource implements SuggestionContract.Model {

    private static final String URL_QUERY_API_DUCKDUCKGO = "https://ac.duckduckgo.com/ac/?q=";
    private static final String URL_SEARCH_DUCKDUCKGO = "https://duckduckgo.com/?q=";

    private AsyncTask queryTask;

    SuggestionDataSource() {
    }

    @Override
    public void query(CharSequence query, final SuggestionContract.QueryReponseListener queryReponseListener) {
        String url = "";
        try {
            url = URLEncoder.encode(query.toString(), "utf-8");
        } catch (UnsupportedEncodingException e) {
        }

        if (queryTask != null) {
            queryTask.cancel(true);
            queryTask = null;
        }
        queryTask = new AsyncTask<String, Void, String[]>() {
            @Override
            protected String[] doInBackground(String... urls) {
                return HttpRequest.get(URL_QUERY_API_DUCKDUCKGO + urls[0]);
            }

            @Override
            protected void onPostExecute(String[] strings) {
                queryReponseListener.onQuerySuccess(strings);
            }
        }.execute(url);
    }

    @Override
    public String searchUrl(CharSequence searchUrl) {
        String url = "";
        try {
            url = URLEncoder.encode(searchUrl.toString(), "utf-8");
        } catch (UnsupportedEncodingException e) {
        }
        return URL_SEARCH_DUCKDUCKGO + url;
    }

    void cancelAll() {
        if (queryTask != null) {
            queryTask.cancel(true);
        }
    }

    void stop() {
        if (queryTask != null) {
            queryTask.cancel(true);
        }
    }

    private static class HttpRequest {

        static String[] get(String uri) {

            String line = "";
            HttpURLConnection urlConnection = null;
            try {
                URL url = new URL(uri);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                BufferedReader r = new BufferedReader(new InputStreamReader(in));
                StringBuilder total = new StringBuilder();
                while ((line = r.readLine()) != null) {
                    total.append(line).append('\n');
                }

                line = total.toString();
            } catch (MalformedURLException e) {
            } catch (IOException e) {
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }

            String[] suggestions = new String[0];
            if (TextUtils.isEmpty(line)) {
                return suggestions;
            }

            try {
                JSONArray jsonArray = new JSONArray(line);
                int size = jsonArray.length();
                ArrayList suggests = new ArrayList<String>(size);
                try {
                    for (int i = 0; i < size; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String suggestText = jsonObject.getString("phrase");
                        suggests.add(suggestText);
                    }
                } catch (JSONException e) {
                }
                suggestions = (String[]) suggests.toArray(new String[suggests.size()]);
            } catch (JSONException e) {
            }

            return suggestions;
        }
    }
}
